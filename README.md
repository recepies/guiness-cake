# Guiness Cake

## Ingredientes
 - 275g harina
 - 350g de azúcar
 - 75g chocolate en polvo
 - 2 cucharadas de bicarbonato (o sobre de levadura)
 - 25cl cerveza Guiness
 - 20cl nata líquida para montar
 - 2 huevos


## Ingredientes frosting
 - 250g queso philadelphia
 - 100g azúcar glas
 - 20cl nata líquida para montar


## Preparación bizcocho

1. Poner el horno a precalentar (180°)
1. Calentar cerveza **sin que llegue a hervir**, justo al punto en el que empieza a echar humo, apagar el fuego añadir mantequilla y remover.
1. Mezclar en un bowl harina, azúcar, cacao y bicarbonato (se puede sustituir el bicarbonato por levadura)
1. En otro bowl batir huevos con nata hasta que salga una crema densa
1. Añadir al bowl de los huevos y la nata, la cerveza con mantequilla primero y el otro bowl con la harina y el azúcar después
1. Hornear a 180º unos 50'

*Se puede sustituir la mitad de la mantequilla por dos cucharadas soperas de aceite*

## Preparación Frosting

Lo mejor es preparar esto poco antes de servir la tarta y **se coloca sobre la tarta fría**.

1. Mezclar azúcar glas y queso en un bowl
1. Montar la nata en otro recipiente y mezclar con lo anterior
1. Colocar sobre la tarta
